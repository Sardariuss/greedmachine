import os
import sys
import json
import datetime
import traceback
import multiprocessing
import shutil
from register import Register
from file_utils import FileUtils
from functools import partial

WIN_RAR_PATH = 'C:\Program Files (x86)\WinRAR\winRAR.exe'
JSON_DEMO_EXTRACTOR_PATH = 'C:\\Users\\Tanguy\\Documents\\GreedMachine\\JSONDemoExtractor\\JSONDemoExtractor\\bin\\Release\\JSONDemoExtractor.exe'

class DataExtractor :

	version = 1.0
	err_extract = "extract"
	err_analyze = "analyze"
	err_info = "info"
	err_unknown = "unknown"

	def __init__(self, archive_dir, data_dir, register) :
		""""Constructor"""
		self.archive_dir = archive_dir
		self.data_dir = data_dir
		self.register = register

	def extract(self) :
		"""Extract data from archives found in self.archive_dir"""
		print('Start extracing (not registred) games in %s from archives in %s' % (self.data_dir, self.archive_dir))
		self.register.load()
		list_archives = os.listdir(self.archive_dir)
		try :
			pool = multiprocessing.Pool(os.cpu_count())
			manager = multiprocessing.Manager()
			lock = manager.Lock()
			func = partial(self.extract_archive, lock)
			pool.map(func, list_archives)
		finally :
			pool.close()
			pool.join()
		print('Finished extracing (not registred) games in %s from archives in %s' % (self.data_dir, self.archive_dir))

	def extract_archive(self, lock, archive) :
		"""Extract data archive located in  self.archive_dir into self.data_dir"""
		if archive.endswith('.rar') or archive.endswith('.zip') :
			match_info = FileUtils.get_match_info(archive)
			match_date = datetime.date(match_info["year"], match_info["month"], match_info["day"])
			team_1 = match_info["team_1"]
			team_2 = match_info["team_2"]
			lock.acquire()
			self.register.load()
			match_analyzed = self.register.is_match_analyzed(match_date, team_1, team_2, DataExtractor.version)
			lock.release()
			if not match_analyzed :
				# Get archive path
				archive_path = os.path.join(self.archive_dir, archive)
				# Create temp demo directory
				demo_dir = os.path.join(self.data_dir, archive[:-4])
				if not os.path.isdir(demo_dir) :
					os.mkdir(demo_dir)
				# Initliaze error
				error = ""
				# Extract the demo files from the archives (overwrite)
				cmd = '""%s" x -ibck -o+ -- "%s" *.dem "%s" 1>nul"' % (WIN_RAR_PATH, archive_path, demo_dir)
				if os.system(cmd)!=0 :
					error = DataExtractor.err_extract
				else :
					# Extract the data from the demo files
					game_id = 1
					for demo in os.listdir(demo_dir) :
						if demo.endswith(".dem"):
							try:
								# Extract the data with JSONDemoExtractor.exe
								cmd = '""%s" "%s" "%s""' % (JSON_DEMO_EXTRACTOR_PATH, os.path.join(demo_dir, demo), demo_dir)
								if os.system(cmd)!=0 :
									error = DataExtractor.err_analyze
									continue
								# Rename json file
								json_file = "%s.json" % demo[:-4]
								json_path = os.path.join(demo_dir, json_file)
								final_json_file = "%s_%d.json" % (archive[:-4], game_id)
								final_json_path = os.path.join(self.data_dir, final_json_file)
								shutil.move(json_path, final_json_path)
								# Set game info in file
								if not FileUtils.set_match_info(final_json_path, match_info) :
									error = DataExtractor.err_info
									continue
							except :
								error = DataExtractor.unknown
							finally :
								game_id += 1
				# Print state of extraction in console
				if (error=="") :
					print('    Successfully extracted games from %s.' % archive)
				else :
					print('    ERROR (%s) in extracting games from %s.' % (error, archive))
				# Save match in register
				lock.acquire()
				self.register.load()
				self.register.add_match(match_date, team_1, team_2, error, DataExtractor.version)
				self.register.dump()
				lock.release()
				# Delete temp demo directory
				shutil.rmtree(demo_dir)

	def update_register(self) :
		"""Warning, function added for convenience: record each data file found in data_dir as a game in the register"""
		self.register.load()
		for file in os.listdir(self.data_dir) :
			if file.endswith('json') and not '(' in file:
				#print("Parse file : %s" % file)
				game_info = FileUtils.get_game_info(file)
				date = datetime.date(game_info["year"], game_info["month"], game_info["day"])
				team_1 = game_info["team_1"]
				team_2 = game_info["team_2"]
				game_id = game_info["game_id"]
				# Record the game only if not already in the register
				if not self.register.is_game_analyzed(date, team_1, team_2, game_id, 0) :
					print('Register game %d %d %d %s %s %d' %(game_info["year"], game_info["month"], game_info["day"], team_1, team_2, game_id))
					# Assume game has no error, version 0
					self.register.add_match(date, team_1, team_2, game_id, False, 0)
		self.register.dump()