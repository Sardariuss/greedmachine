import datetime
from register import Register
from archive_scraper import ArchiveScraper
from data_extractor import DataExtractor

if __name__ == "__main__" :

	ARCHIVE_DIR = 'F:\\GM\\archives'
	REGISTER_PATH = "D:\\GM\\register.json"
	DATA_DIR = 'D:\\GM\\data'
	# Check the archives over the past month
	END_DATE = datetime.date.today()
	START_DATE =  datetime.date(END_DATE.year, END_DATE.month, 1)
	
	register = Register(REGISTER_PATH)

	archive_scraper = ArchiveScraper(ARCHIVE_DIR, register)
	archive_scraper.scrape_hltv(START_DATE, END_DATE)
	data_extractor = DataExtractor(ARCHIVE_DIR, DATA_DIR, register)
	data_extractor.extract()
