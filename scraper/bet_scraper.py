import datetime
import os.path
import traceback
import re
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from clint.textui import progress

from config import phantomjs_path
from json_handler import JsonHandler, JsonBetHandler


class BetScraper :

	json_bet_types = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'bet_types.json')

	def __init__(self) :
		""""""
		self.bet_type_handler = JsonHandler(BetScraper.json_bet_types)
		self.bet_site = ''
		self.num_types = 0

	def add_bet_type(self, bet, states) :
		""""""
		bet_dict = {'bet':bet, 'states': sorted(states[:])}
		try :
			if bet_dict not in self.bet_type_handler.dict[self.bet_site] :
				self.num_types += 1
				self.bet_type_handler.dict[self.bet_site].append(bet_dict)
		except KeyError:
			self.bet_type_handler.dict[self.bet_site] = [bet_dict]

	def run(self) :
		""""""
		print("Scrape odds from %s..." % self.bet_site)
		self.bet_type_handler.load()
		self.run_specialized()
		if self.num_types > 0 :
			print('%d new types of bets have been scraped from %s.' % (self.num_types, self.bet_site))
		self.bet_type_handler.dump()

	def run_specialized(self) :
		""""""
		pass

class FanobetScraper(BetScraper) :

	json_fanobet = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'fanobet.json')

	def __init__(self) :
		""""""
		super(FanobetScraper, self).__init__()
		self.fanobet_handler = JsonBetHandler(FanobetScraper.json_fanobet)
		self.bet_site = 'fanobet.com'

	def run_specialized(self) :
		""""""
		# Main window
		url_fanobet = "https://fanobet.com/#/game/cs-go"
		xpath_upcoming_matches = '//*[@id="main-content"]/div[2]/div[4]/div'
		# Relative to xpath_upcoming_matches
		xpath_match_bo = './ng-include/div/div[1]/a/div[1]/span'
		xpath_match_map = './ng-include/div/div[2]/span'
		xpath_match_anchor = './ng-include/div/div[1]/a'
		# Match window
		xpath_main = '//*[@id="main-content"]/div/div'
		# Relative to xpath_main
		xpath_event = './div[2]/div[1]'
		xpath_date_time = './div[2]/div[2]/span'
		xpath_team_1 = './div[3]/div[1]/div[2]/span'
		xpath_team_2 = './div[3]/div[3]/div[2]/span'
		xpath_odd_1 = './div[4]/div[1]/div/span'
		xpath_odd_2 = './div[4]/div[2]/div/span'
		num_div_bet = 5
		xpath_bets = './div'
		# Relative to xpath_bets
		xpath_bet_type = './span'
		xpath_bet_states = './div/div'
		# Relative to xpath_bet_states
		xpath_bet_state = './span[1]'
		xpath_bet_state_odd = './span[2]'
		# Load fanobet.json
		self.fanobet_handler.load()
		# For debug purpose
		trace_list = []
		try:
			driver = webdriver.PhantomJS(phantomjs_path, service_log_path=os.path.devnull)
			# Resize window otherwise some content may be trimmed
			driver.set_window_size(1280,800)
			driver.get(url_fanobet)
			for elem_match in progress.bar(driver.find_elements_by_xpath(xpath_upcoming_matches)) :
				dict_match = {}
				# Driver for the match window
				driver_match = webdriver.PhantomJS(phantomjs_path, service_log_path=os.path.devnull)
				try :
					# First get bo and map information from the main window
					dict_match['bo'] = elem_match.find_element_by_xpath(xpath_match_bo).text
					try :
						dict_match['map'] = elem_match.find_element_by_xpath(xpath_match_map).text
					except NoSuchElementException:
						dict_match['map'] = ''
					# Resize window otherwise some content may be trimmed
					driver_match.set_window_size(1280,800)
					driver_match.get(elem_match.find_element_by_xpath(xpath_match_anchor).get_attribute('href'))
					# Main element from the match window
					elem_main = driver_match.find_element_by_xpath(xpath_main)
					# e.g. : 20:15 10/02
					# @todo: day as a zero-padded decimal, could not verify, it may be without
					date_match = datetime.datetime.strptime(elem_main.find_element_by_xpath(xpath_date_time).text, '%H:%M %d/%m')
					date_run = datetime.datetime.today()
					date_match = date_match.replace(year=date_run.year)
					if date_match < date_run :
						date_match = date_match.replace(year=date_run.year+1)
					dict_match['datetime'] = date_match.isoformat()
					dict_match['event'] = elem_main.find_element_by_xpath(xpath_event).text
					dict_match['team1'] = elem_main.find_element_by_xpath(xpath_team_1).text
					dict_match['team2'] = elem_main.find_element_by_xpath(xpath_team_2).text
					dict_match['odd1'] = float(elem_main.find_element_by_xpath(xpath_odd_1).text)
					dict_match['odd2'] = float(elem_main.find_element_by_xpath(xpath_odd_2).text)
					dict_match['bets'] = {}
					# Iterate over the web elements containing the extra bets
					for elem_bet in elem_main.find_elements_by_xpath(xpath_bets)[num_div_bet-1:] :
						# Set bet type in bet types dictionary
						bet_type = elem_bet.find_element_by_xpath(xpath_bet_type).text
						bet_type = re.sub(dict_match['team1'], 'team1', bet_type, flags=re.I)
						bet_type = re.sub(dict_match['team2'], 'team2', bet_type, flags=re.I)
						# Set bet type as extra bet in match dictionary
						dict_match['bets'][bet_type] = {}
						bet_states = []
						# Iterate over the web elements containing the possible bet states
						for elem_bet_state in elem_bet.find_elements_by_xpath(xpath_bet_states) :
							# Get bet state
							bet_state = elem_bet_state.find_element_by_xpath(xpath_bet_state).text
							bet_state = re.sub(dict_match['team1'], 'team1', bet_state, flags=re.I)
							bet_state = re.sub(dict_match['team2'], 'team2', bet_state, flags=re.I)
							bet_states.append(bet_state)
							# Get bet odd for this state
							dict_match['bets'][bet_type][bet_state] = float(elem_bet_state.find_element_by_xpath(xpath_bet_state_odd).text)
						# Add bet type
						BetScraper.add_bet_type(self, bet_type, bet_states)
					# Add match
					self.fanobet_handler.add(dict_match)
				except:
					trace_list.append(traceback.format_exc(limit=1))
				finally:
					driver_match.quit()
		except:
			print(traceback.format_exc())
		finally:
			self.fanobet_handler.dump()
			driver.quit()
			# Log errors if any
			if len(trace_list) > 0 :
				print('Warning: %d match(es) could not be scraped properly. Traceback(s):' % len(trace_list))
				for trace in list(set(trace_list)) :
					print(trace)

if __name__=="__main__" :
	"""main"""
	fanobet_scraper = FanobetScraper()
	fanobet_scraper.run()
