import os
from selenium import webdriver
import traceback

from config import phantomjs_path
from json_handler import JsonHandler

# @todo: could use a simple list instead of dict with team/player id, the id would 
# be the position in the list anyway, but maybe this is safer with a dict...

# @doit: sometimes team name is empty while it is not the last team... deal with it

class TeamScraper : 

	json_teams = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'teams.json')

	def __init__(self) :
		"""Constructor"""
		self.teams_handler = JsonHandler(TeamScraper.json_teams)

	def run(self) :
		"""Scrape teams and players from hltv.org"""
		# Relative to team page
		url_team = 'http://www.hltv.org/?pageid=179&teamid=%d'
		xpath_team_name = '//*[@id="back"]/div[3]/div[3]/div/div[2]/div[2]/div[1]/div[2]'
		# Relative to player page
		url_player = 'http://www.hltv.org/?pageid=173&statsfilter=0&playerid=%d'
		xpath_player_name = '//*[@id="back"]/div[3]/div[3]/div/div[2]/div[2]/div[1]/div[2]'
		# Load teams.json
		self.teams_handler.load()
		# Init driver
		driver = webdriver.PhantomJS(phantomjs_path, service_log_path=os.path.devnull)
		print('Scrape teams and players from hltv.org...')
		try :
			# Scrape teams
			team_id = 1
			if 'teams' not in list(self.teams_handler.dict.keys()) :
				# Add 'teams' in dict if it is not already in (first time scraper run)
				self.teams_handler.dict['teams'] = {}
			else :
				# Get last team scraped
				team_id = int(max(key for key, team in self.teams_handler.dict['teams'].items())) + 1
			while True :
				print('Scrape team %d' % team_id)
				driver.get(url_team % team_id)
				team_name = driver.find_element_by_xpath(xpath_team_name).text
				# In case there is no team with this id, stop here
				if team_name=='' :
					break
				self.teams_handler.dict['teams'][team_id] = team_name
				team_id += 1
			# Scrape players
			player_id = 1
			if 'players' not in list(self.teams_handler.dict.keys()) :
				# Add 'players' in dict if it is not already in (first time scraper run)
				self.teams_handler.dict['players'] = {}
			else :
				# Get last player scraped
				player_id = int(max(key for key, player in self.teams_handler.dict['players'].items())) + 1
			while True :
				print('Scrape player %d' % player_id)
				driver.get(url_player % player_id)
				player_name = driver.find_element_by_xpath(xpath_player_name).text
				# In case there is no player with this id, stop here
				if player_name == 'N/A' :
					break
				self.teams_handler.dict['players'][player_id] = player_name
				player_id += 1
		except :
			print(traceback.format_exc())
		finally :
			driver.quit()
			self.teams_handler.dump()

if __name__=='__main__' :
	"""main"""
	team_scraper = TeamScraper()
	team_scraper.run()
