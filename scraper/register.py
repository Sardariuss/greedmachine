import json
import datetime

class Match :

	# TODO: replace add_year, add_month, add_day by add_match

	def __init__(self, match_dict=None, team_id_1=-1, team_id_2=-1, error="", version=0.0) :
		"""Constructor"""
		if match_dict!=None:
			self.from_dict(match_dict)
		else :
			self.team_id_1 = team_id_1
			self.team_id_2 = team_id_2
			self.error = error
			self.version = version

	def from_dict(self, match_dict) :
		"""Initialize match from dictionary"""
		self.team_id_1 = match_dict["team_id_1"]
		self.team_id_2 = match_dict["team_id_2"]
		self.error = match_dict["error"]
		self.version = match_dict["version"]

	def to_dict(self) :
		"""Return match dictionary"""
		return {"team_id_1" : self.team_id_1, "team_id_2" : self.team_id_2, "error" : self.error, "version" : self.version}

class Day :

	def __init__(self, day_dict=None, day=0, matches=[]) :
		"""Constructor"""
		if day_dict!=None :
			self.from_dict(day_dict)
		else :
			self.day = day
			self.matches = matches

	def from_dict(self, day_dict) :
		"""Initialize day from dictionary"""
		self.day = day_dict["day"]
		self.matches = [Match(match_dict=match_dict) for match_dict in day_dict["matches"]]

	def to_dict(self) :
		"""Return day dictionary"""
		return {"day" : self.day, "matches" : [match.to_dict() for match in self.matches]}

	def get_match(self, team_id_1, team_id_2) :
		"""Get match opposing the two teams if any during this day"""
		for i in range(len(self.matches)) :
			if self.matches[i].team_id_1==team_id_1 and self.matches[i].team_id_2==team_id_2 :
				return self.matches[i]
		return None

	def add_match(self, match) :
		"""Add match to this day"""
		# If this match is already in the list, just refresh it
		for i in range(len(self.matches)) :
			if self.matches[i].team_id_1==match.team_id_1 and self.matches[i].team_id_2==match.team_id_2 :
				self.matches[i].error = match.error
				self.matches[i].version = match.version
				return
		# Else add this match to the list
		self.matches.append(match)

class Month :

	def __init__(self, month_dict=None, month=0, days=[]) :
		"""Constructor"""
		if month_dict!=None :
			self.from_dict(month_dict)
		else :
			self.month = month
			self.days = days

	def from_dict(self, month_dict) :
		"""Initialize month from dictionary"""
		self.month = month_dict["month"]
		self.days = [Day(day_dict=day_dict) for day_dict in month_dict["days"]]

	def to_dict(self) :
		"""Return month dictionary"""
		return {"month" : self.month, "days" : [day.to_dict() for day in self.days]}

	def get_day(self, day_number) :
		"""Get day of this month"""
		for i in range(len(self.days)) :
			if self.days[i].day==day_number :
				return self.days[i]
		return None

	def add_day(self, day) :
		"""Add day to this month"""
		# If this day is already in the list, add matches to this day
		for i in range(len(self.days)) :
			if self.days[i].day==day.day :
				for match in day.matches :
					self.days[i].add_match(match)
				return
		# Else add this day to the list
		self.days.append(day)

class Year :

	def __init__(self, year_dict=None, year=0, months=[]) :
		"""Constructor"""
		if year_dict!=None :
			self.from_dict(year_dict)
		else :
			self.year = year
			self.months = months

	def from_dict(self, year_dict) :
		"""Initialize year from dictionary"""
		self.year = year_dict["year"]
		self.months = [Month(month_dict=month_dict) for month_dict in year_dict["months"]]

	def to_dict(self) :
		"""Return year dictionary"""
		return { "year" : self.year, "months" : [month.to_dict() for month in self.months]}

	def get_month(self, month_number) :
		"""Get month of this year"""
		for i in range(len(self.months)) :
			if self.months[i].month == month_number :
				return self.months[i]
		return None

	def add_month(self, month) :
		"""Add month to this year"""
		# If this month is already in the list, add days to this month
		for i in range(len(self.months)) :
			if self.months[i].month == month.month :
				for day in month.days :
					self.months[i].add_day(day)
				return
		# Else add this month to the list
		self.months.append(month)

class Team :

	def __init__(self, team_dict=None, id=-1, name="") :
		"""Constructor"""
		if team_dict!=None:
			self.from_dict(team_dict)
		else:
			self.id = id
			self.name = name

	def __eq__(self, other):
		"""Return true if teams' names match"""
		if isinstance(other, self.__class__):
			return self.name==other.name
		return NotImplemented

	def __ne__(self, other):
		"""Return false if teams' names do not match"""
		if isinstance(other, self.__class__):
			return not self.__eq__(other)
		return NotImplemented

	def from_dict(self, team_dict) :
		"""Initialize team from dictionary"""
		self.id = team_dict["id"]
		self.name = team_dict["name"]

	def to_dict(self) :
		"""Return team dictionary"""
		return { "id" : self.id, "name" : self.name }

class Register :

	def __init__(self, json_path) :
		"""Constructor"""
		self.json_path = json_path
		self.teams = []
		self.years = []

	def from_dict(self, register_dict) :
		"""Initialize register from dictionary"""	
		self.teams = [Team(team_dict=team_dict) for team_dict in register_dict["teams"]]
		self.years = [Year(year_dict=year_dict) for year_dict in register_dict["years"]]

	def to_dict(self) :
		"""Return register dictionary"""
		return { "teams" : [team.to_dict() for team in self.teams], "years" : [year.to_dict() for year in self.years]}

	def get_year(self, year_number) :
		"""Return the year from this register"""
		for i in range(len(self.years)) :
			if self.years[i].year == year_number :
				return self.years[i]
		return None

	def add_year(self, year) :
		"""Add the year to this register"""
		# If this year is already in the list, add months to this year
		for i in range(len(self.years)) :
			if self.years[i].year == year.year :
				for month in year.months :
					self.years[i].add_month(month)
				return
		# Else add this year to the list
		self.years.append(year)

	def get_team(self, team_name) :
		"""Get the team from this register"""
		for team in self.teams :
			if team.name==team_name :
				return team
		return None

	def add_team(self, team_name) :
		"""Add team to this register"""
		# Do not add the team if there is already a team with this name
		team = Team(id=len(self.teams), name=team_name)
		if team not in self.teams :
			self.teams.append(team)

	def get_match(self, date, team_name_1, team_name_2) :
		"""Get match from this register"""
		team_1 = self.get_team(team_name_1)
		team_2 = self.get_team(team_name_2)
		if team_1!=None and team_2!=None:
			year = self.get_year(date.year)
			if year!=None :
				month = year.get_month(date.month)
				if month!=None :
					day = month.get_day(date.day)
					if day!=None :
						return day.get_match(team_1.id, team_2.id)
		return None

	def add_match(self, date, team_name_1, team_name_2, error="", version=0.0) :
		"""Add match to this register"""
		self.add_team(team_name_1)
		self.add_team(team_name_2)
		team_1=self.get_team(team_name_1)
		team_2=self.get_team(team_name_2)
		if team_1!=None and team_2!=None:
			match=Match(team_id_1=team_1.id, team_id_2=team_2.id, error=error, version=version)
			day = Day(day=date.day, matches=[match])
			month = Month(month=date.month, days=[day])
			year = Year(year=date.year, months=[month])
			self.add_year(year)

	def is_match_downloaded(self, date, team_name_1, team_name_2) :
		"""Return true if match is in the register"""
		return self.get_match(date, team_name_1, team_name_2)!=None
	
	def is_match_analyzed(self, date, team_name_1, team_name_2, version) :
		"""Return true if match is in the register and has up-to-date games"""
		match = self.get_match(date, team_name_1, team_name_2)
		return (match!=None and match.version>=version and match.error=="")

	def load(self) :
		""""Load register from json_path"""
		with open (self.json_path, 'r+', encoding="utf8") as file_handle :
			try :
				json_dict = json.load(file_handle)
			except ValueError :
				json_dict = {"teams" : [], "years" : []}
			finally:
				self.from_dict(json_dict)

	def dump(self) :
		"""Dump register to json_path"""
		with open (self.json_path, 'r+', encoding="utf8") as file_handle :
			json.dump(self.to_dict(), file_handle, indent="  ", sort_keys=True)
