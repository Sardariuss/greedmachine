import json
import editdistance

class FileUtils :

	TEAM_NAME_DIST_THR = 3

	@staticmethod
	def get_match_info(archive_file) :
		""" Requires file format like year_month_day_team1VSteam2.rar """		
		info = {}
		# Keeps only alphagame_ideric characters (and underscores)
		#archive_file = re.sub(r'\W+', '', json_file)
		# Remove first part and last part
		underscore_pos = FileUtils.find_all(archive_file, '_')
		vs_pos = archive_file.find('VS')
		# Get date and teams
		info["year"] = int(archive_file[:underscore_pos[0]])
		info["month"] = int(archive_file[underscore_pos[0]+1:underscore_pos[1]])
		info["day"] = int(archive_file[underscore_pos[1]+1:underscore_pos[2]])
		info["team_1"] = archive_file[underscore_pos[2]+1:vs_pos]
		info["team_2"] = archive_file[vs_pos+2:len(archive_file)-4]
		return info

	@staticmethod		
	def get_game_info(data_file) :
		""" Requires file format like year_month_day_team1VSteam2_1.json """
		info = {}
		# Keeps only alphagame_ideric characters (and underscores)
		#data_file = re.sub(r'\W+', '', json_file)
		# Remove first part and last part
		underscore_pos = FileUtils.find_all(data_file, '_')
		vs_pos = data_file.find('VS')
		# Get date and teams
		info["year"] = int(data_file[:underscore_pos[0]])
		info["month"] = int(data_file[underscore_pos[0]+1:underscore_pos[1]])
		info["day"] = int(data_file[underscore_pos[1]+1:underscore_pos[2]])
		info["team_1"] = data_file[underscore_pos[2]+1:vs_pos]
		info["team_2"] = data_file[vs_pos+2:underscore_pos[3]]
		info["game_id"] = int(data_file[underscore_pos[3]+1:len(data_file)-5])
		return info

	@staticmethod
	def set_match_info(data_path, match_info) :
		""" Use to update the date and teams names in the raw data"""
		with open (data_path, 'r+', encoding="utf-8") as file_handle:
			json_data = json.load(file_handle)
			# Set date
			str_date = "%s-%s-%s" %(match_info["day"], match_info["month"], match_info["year"])
			json_data["date"] = str_date
			# Check team names
			team_ct = json_data["team_ct"]["team_name"].lower()
			team_t = json_data["team_t"]["team_name"].lower()
			team_1 = match_info["team_1"]
			team_2 = match_info["team_2"]
			if team_1 in team_ct or team_2 in team_t :
				json_data["team_ct"]["team_name"] = team_1
				json_data["team_t"]["team_name"] = team_2
			elif team_1 in team_t or team_2 in team_ct :
				json_data["team_ct"]["team_name"] = team_2
				json_data["team_t"]["team_name"] = team_1
			else :
				# Compare team names
				dist_1_ct = editdistance.eval(team_1, team_ct)
				dist_1_t = editdistance.eval(team_1, team_t)
				dist_2_ct = editdistance.eval(team_2, team_ct)
				dist_2_t = editdistance.eval(team_2, team_t)
				min_dist = min(dist_1_ct, dist_1_t, dist_2_ct, dist_2_t)
				# Consider it is too huge of a risk if difference is above threshold
				if (min_dist > FileUtils.TEAM_NAME_DIST_THR) :
					print("Exception occured in set_match_info: could not set teams' name in file")
					print("    archive: %s" % data_path)
					print("    source: %s" % json_data["source"])
					print("    team_ct: %s" % team_ct)
					print("    team_t: %s" % team_t)
					print("    team_1: %s" % team_1)
					print("    team_2: %s" % team_2)
					return False
				# Use the "closest" team name
				# POTENTIAL DATA BIAS HERE IF TEAMS NAME ARE TEAM1 AND TEAM2 IN FILE
				if (min_dist == (dist_1_ct or dist_2_t)) :
					json_data["team_ct"]["team_name"] = team_1
					json_data["team_t"]["team_name"] = team_2
				else :
					json_data["team_ct"]["team_name"] = team_2
					json_data["team_t"]["team_name"] = team_1
			# Edit file
			file_handle.seek(0)
			json.dump(json_data, file_handle, indent="  ", sort_keys=True)
			file_handle.truncate()
			return True

	@staticmethod
	def find_all(a_str, sub) :
		"""Return the positions of the substring in the string."""
		start = 0
		indices = []
		while True:
			start = a_str.find(sub, start)
			if start == -1:
				break
			indices.append(start)
			start += len(sub) # use start += 1 to find overlapping matches
		return indices
