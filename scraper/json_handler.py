import json
import os.path

# Ease the file manipulation : 
#   - Create the file if it does not exist
#   - Save it (actually overwrite the file)
# @todo: does not work if json is a list
class JsonHandler :

	def __init__(self, json_file) :
		"""Constructor"""
		self.file = json_file
		self.dict = {}

	def load(self) :
		""""""
		file_mode = 'r' if os.path.exists(self.file) else 'a'
		with open(self.file, file_mode, encoding='utf8') as json_file :
			try:
				self.dict = json.load(json_file)
			except ValueError:
				# If file is not empty and content could not be loaded, abort
				if file_mode == 'r' :
					raise NameError('Failed to load the file %s. Abort.' % self.file)

	def dump(self, sort_keys=False) :
		""""""
		with open(self.file, 'w', encoding='utf8') as json_file :
			json.dump(self.dict, json_file, indent="  ", sort_keys=sort_keys)


# Create hash key to:
#   - Search faster
#   - Check/update the 'unique' elements (here the matches)
class JsonBetHandler(JsonHandler) :

	keys = ['datetime', 'team1', 'team2', 'map']

	def __init__(self, json_file) :
		"""Constructor"""
		super(JsonBetHandler, self).__init__(json_file)

	def load(self) :
		"""Create a dict indexed with joined keys from the loaded dict (which is actually a list here)"""
		super(JsonBetHandler,self).load()
		self.dict = dict((''.join(sub_dict[key] for key in JsonBetHandler.keys), sub_dict) for sub_dict in self.dict)
			
	def add(self, sub_dict) :
		""""""
		self.dict[''.join(sub_dict[key] for key in JsonBetHandler.keys)] = sub_dict

	def dump(self, sort_keys=False) :
		""""""
		with open(self.file, 'w', encoding='utf8') as file :
			# Reshape the dictionary as it has been loaded
			json.dump(list(self.dict.values()), file, indent="  ", sort_keys=sort_keys)
