import re
import time
import os.path
import requests
import datetime
import traceback
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from clint.textui import progress
from timeit import default_timer as timer

from config import phantomjs_path, archive_dir
from json_handler import JsonHandler


class DemoScraper :

	matches_url = 'http://www.hltv.org/matches/archive/'
	prefix_match_url = 'http://www.hltv.org/match/'
	json_demos = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'demos.json')
	download_timeout = 600

	def __init__(self, archive_dir) :
		""""Constructor"""
		self.archive_dir = archive_dir
		self.demos_handler = JsonHandler(DemoScraper.json_demos)

	def run(self):
		""""""
		print("Scrape demos from hltv.org...")
		id_calendar = 'calendar'
		id_has_demo = 'hasDemo'
		id_search = 'search'
		xpath_matches = "//a[contains(@href, '/match/') and @style='color: black;']"
		xpath_date = "//span[@style='font-size:14px;']"
		xpath_match = "//title"
		xpath_demo = "//a[contains(., 'GOTV')]"
		xpath_match_url = "//a[contains(@href, 'interfaces/download')]"
		xpath_next_page = "//a[contains(@href, 'javascript:nextPage(%d);')]"
		# Load demos.json
		self.demos_handler.load()
		# Web driver main page
		driver = webdriver.PhantomJS(phantomjs_path, service_log_path=os.path.devnull)
		driver.get(DemoScraper.matches_url)
		driver.implicitly_wait(10)
		# Set date range
		date_range = max(key for key, list_demos in self.demos_handler.dict.items()) + ' to ' + datetime.date.today().isoformat()
		calendar_element = driver.find_element_by_id(id_calendar)
		calendar_element.clear()
		calendar_element.send_keys(date_range)
		time.sleep(2)
		# Check demo only
		has_demo_element = driver.find_element_by_id(id_has_demo)
		has_demo_element.click()
		time.sleep(2)
		# Start the research
		search_element = driver.find_element_by_id(id_search)
		search_element.click()
		time.sleep(8)
		# Iterate over the pages
		num_page = 1
		try :
			while True :
				# Get the list of urls
				match_urls = [match.get_attribute('href') for match in driver.find_elements_by_xpath(xpath_matches)]
				# Iterate over the urls
				for match_url in match_urls :
					print ('Scrape demo at url: %s' % match_url)
					try :
						# Match page
						driver_match = webdriver.PhantomJS(phantomjs_path)
						driver_match.get(match_url)
						sub_url = re.sub(DemoScraper.prefix_match_url, '', match_url)
						match_date = self.parse_hltv_date(driver_match.find_element_by_xpath(xpath_date).get_attribute('innerText'))
						# Check if demo has already been scraped
						try :
							for demo in self.demos_handler.dict[match_date] :
								if demo['url']==sub_url:
									raise NameError('Demo has already been scraped.')
						except KeyError :
							self.demos_handler.dict[match_date] = []
						time.sleep(1)
						# Get match name
						match = driver_match.find_element_by_xpath(xpath_match).get_attribute('innerText')
						match = match[:match.find(' at')]
						match = match.replace(' ', '')
						match = match.lower()
						team_1 = match[:match.find('vs')]
						team_2 = match[match.find('vs')+2:]
						archive = '%s_%sVS%s.rar' % (match_date, team_1, team_2)
						archive_path = os.path.join(self.archive_dir, archive)
						if (os.path.isfile(archive_path)) :
							raise NameError('File %s does already exist.' % archive)
						# Open match page
						elem_match = driver_match.find_element_by_xpath(xpath_demo)
						elem_match.click()
						time.sleep(1)
						# Download demo
						download_url = driver_match.find_element_by_xpath(xpath_match_url).get_attribute('href')
						self.download_archive(download_url, archive_path)
						self.demos_handler.dict[match_date].append({'url':sub_url, 'team1':team_1, 'team2':team_2, 'archive':archive})
					except :
						print('Could not scraped demo. %s' % traceback.format_exc(limit=1))
					finally:
						driver_match.quit()
						self.demos_handler.dump()
				# Get to next page if any, there are 100 matches per page
				try :
					driver.find_element_by_xpath(xpath_next_page % (num_page * 100)).click()
					num_page += 1
				except NoSuchElementException :
					break
		except :
			print(traceback.format_exc())
		finally :
			driver.quit()

	def download_archive(self, url, archive_path):
		"""Download archive at given url in archive_dir"""
		r = requests.get(url, stream=True)
		timed_out = False
		with open(archive_path, 'wb') as f:
			total_length = int(r.headers.get('content-length'))
			start = timer()
			for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
				if timer()-start > DemoScraper.download_timeout :
					timed_out = True
					break
				if chunk:
					f.write(chunk)
					f.flush()
		if timed_out :
			os.remove(archive_path)
			raise NameError('Download timed out (>%ss).' % DemoScraper.download_timeout)

	def parse_hltv_date(self, date_hltv) :
		"""Get iso date from hltv format (e.g. 2nd of March 2016)"""
		date_hltv = date_hltv.replace('st', '')
		date_hltv = date_hltv.replace('nd', '')
		date_hltv = date_hltv.replace('rd', '')
		date_hltv = date_hltv.replace('th', '')
		date_hltv = date_hltv.replace('of', '')
		date_hltv = date_hltv.replace(' ', '')
		date_hltv = date_hltv.replace('Augu', 'August')
		return datetime.datetime.strptime(date_hltv, '%d%B%Y').date().isoformat()

if __name__=='__main__' :
	"""main"""
	demo_scraper = DemoScraper(archive_dir)
	demo_scraper.run()
