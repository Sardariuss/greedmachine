import datetime

class GameStats : 

	def __init__(self, game_dict=None, date=None, map="", teams=None, features=None, winner=-1) :
		"""Constructor"""
		if game_dict!=None :
			self.from_dict(game_dict)
		else :
			self.date = date
			self.map = map
			self.teams = teams if teams!=None else []
			self.features = features if features!=None else []
			self.winner = winner

	def from_dict(self, game_dict) :
		"""Initialize game stats from dictionary"""
		self.date = datetime.datetime.strptime(game_dict["date"], '%d-%m-%Y')
		self.map = game_dict["map"]
		self.teams = game_dict["teams"]
		self.features = game_dict["features"]
		self.winner = game_dict["winner"]

	def to_dict(self) :
		"""Return game stats"""
		return {"date" : self.date.strftime('%d-%m-%Y'), "map" : self.map, "teams" : self.teams, "features" : self.features, "winner" : self.winner}
		