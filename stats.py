import io
import os
import json
import statistics
import re
import traceback
import datetime
import numpy
from game_stats import GameStats
from team_stats import TeamStats

class Stats : 

	# In JSON files, team_ct seems to reference the team1
	round_ct_team_1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,11, 12, 13, 14, 30, 31, 32, 36, 37, 38, 
		42, 43, 44, 48, 49, 50, 54, 55, 56, 60, 61, 62, 66, 67, 68, 72, 73, 74, 78, 79, 80]
	min_round_count = 15
	# Assume round time is 130 (1min15secs + buy time + end round time)
	time_round = 130
	# Assume the bomb takes 40 seconds to explode ( see http://gaming.stackexchange.com/questions/120218/how-many-seconds-count-until-bomb-detonates-in-counter-strike )
	time_bomb_explosion = 40
	# Required to average f_bp_time_given
	time_penality_no_bp = 200
	# Current number of features, must match the len(features_1) and len(features_2) in function get_game_stats
	number_of_features = 19

	def __init__(self) :
		"""Constructor"""
		self.games = []
		self.teams = []

	def load(self, stats_path) :
		"""Initialize stats from json at stats_path"""
		with io.open (stats_path, 'r+', encoding="utf8") as file_handle :
			try :
				json_dict = json.load(file_handle)
			except ValueError :
				json_dict = {"games" : [], "teams" : []}
			finally:
				self.from_dict(json_dict)

	def dump(self, stats_path) :
		"""Dump stats as json at stats_path"""
		if os.path.isfile(stats_path) :
			print("    File %s already exist, stats dump aborted!" % stats_path)
			return
		with open (stats_path, 'w', encoding="utf8") as file_handle :
			json.dump(self.to_dict(), file_handle, indent="  ", sort_keys=True)

	def to_dict(self) :
		"""Return stats as a dictionary"""
		return {"games" : [game.to_dict() for game in self.games], "teams" : [team.to_dict() for team in self.teams]}

	def from_dict(self, stats_dict) :
		"""Initialize stats from dictionary"""
		self.games = [GameStats(game_dict=game_dict) for game_dict in stats_dict["games"]]
		self.teams = [TeamStats(team_dict=team_dict) for team_dict in stats_dict["teams"]]

	def extract_from_files(self, data_dir) :
		"""Extract stats from files in data_dir"""
		# Iterate over the files containing the raw data and extract the stats
		for file in os.listdir(data_dir) :
			if file.endswith(".json") :
				print("Extract stats from %s" % file)
				with io.open (os.path.join(data_dir, file), 'r+', encoding="utf8") as file_handle :
					try :
						raw_data = json.load(file_handle)
						game = self.get_game_stats(raw_data)
						self.games.append(game)
					except :
						print(traceback.format_exc())
						print("    Failed to extract stats from %s" % file )
						continue
		# Reorder the games by date
		sorted(self.games, key=lambda game: game.date)
		# Add games to teams
		for game_index in range(len(self.games)) :
			# Add game to team 1
			team_name_1 = self.games[game_index].teams[0]
			if team_name_1 not in [team.name for team in self.teams] :
				self.teams.append(TeamStats(name=team_name_1))
			for team_index in range(len(self.teams)) :
				if self.teams[team_index].name == team_name_1 :
					self.teams[team_index].games.append(game_index)
					break
			# Add game to team 2
			team_name_2 = self.games[game_index].teams[1]
			if team_name_2 not in [team.name for team in self.teams] :
				self.teams.append(TeamStats(name=team_name_2))
			for team_index in range(len(self.teams)) :
				if self.teams[team_index].name == team_name_2 :
					self.teams[team_index].games.append(game_index)
					break

	def get_game_stats(self, raw_data) :
		"""Get game statistics from raw_data"""
		date_str = ""
		team_name_1 = ""
		team_name_2 = ""
		map_name = ""
		score_team_1 = 0
		score_team_2 = 0
		team_players_steamid_1 = []
		team_players_steamid_2 = []
		kill_count_1 = 0
		kill_count_2 = 0
		death_count_1 = 0
		death_count_2 = 0
		hs_count_1 = 0
		hs_count_2 = 0
		tk_count_1 = 0
		tk_count_2 = 0
		players_skill_1 = []
		players_skill_2 = []
		ct_round_count_1 = 0
		ct_round_count_2 = 0
		eco_count_1 = 0
		eco_win_count_1 = 0
		eco_count_2 = 0
		eco_win_count_2 = 0
		bp_count_1 = 0
		bp_count_2 = 0
		bp_time_1 = 0
		bp_time_2 = 0
		be_count_1 = 0
		be_count_2 = 0
		win_time_1 = 0
		win_time_2 = 0
		round_index = 0
		team_ct = "unknown"
		# Get date
		date_str = raw_data["date"]
		# Get data from score
		score_team_1 = raw_data["score_team1"]
		score_team_2 = raw_data["score_team2"]
		# Get map
		map_name = raw_data["map_name"]
		# Get data from team_ct and team_t
		team_1 = raw_data["team_ct"]
		team_2 = raw_data["team_t"]
		team_name_1 = team_1["team_name"]
		team_name_2 = team_2["team_name"]
		for team_player_1 in team_1["team_players"] :
			team_players_steamid_1.append(team_player_1["steamid"])
		for team_player_2 in team_2["team_players"] :
			team_players_steamid_2.append(team_player_2["steamid"])
		# Get data from players
		for player in raw_data["players"] :
			if (player["steamid"] in team_players_steamid_1) :
				kill_count_1 += player["kill_count"]
				death_count_1 += player["death_count"]
				hs_count_1 += player["hs_count"]
				tk_count_1 += player["trade_kill_count"]
				players_skill_1.append(player["hltv_rating"])
			else :
				kill_count_2 += player["kill_count"]
				death_count_2 += player["death_count"]
				hs_count_2 += player["hs_count"]
				tk_count_2 += player["trade_kill_count"]
				players_skill_2.append(player["hltv_rating"])
		# Get data from rounds
		for cs_round in raw_data["rounds"] :
			# Round number/side
			if round_index in Stats.round_ct_team_1 :
				ct_round_count_1 += 1
				team_ct = team_name_1
			else : 
				ct_round_count_2 += 1
				team_ct = team_name_2
			# Eco rounds
			if cs_round["type_as_string"] == "Eco" :
				if cs_round["start_money_team1"] < cs_round["start_money_team2"] :
					eco_count_1 += 1
					if cs_round["winner_name"] == team_name_1 :
						eco_win_count_1 += 1
				else :
					eco_count_2 +=1
					if cs_round["winner_name"] == team_name_2 :
						eco_win_count_2 += 1
			# Bomb plant
			bomb_planted = cs_round["bomb_planted"]
			if bomb_planted != None :
				if team_ct == team_name_1 :
					bp_count_2 += 1
					be_count_2 += (0 if cs_round["bomb_exploded"]==None else 1)
					bp_time_2 += bomb_planted["seconds"] - cs_round["start_seconds"]
				else :
					bp_count_1 += 1
					be_count_1 += (0 if cs_round["bomb_exploded"]==None else 1)
					bp_time_1 += bomb_planted["seconds"] - cs_round["start_seconds"]
			else :
				if team_ct == team_name_1 :
					bp_time_2 += Stats.time_penality_no_bp 
				else :
					bp_time_1 += Stats.time_penality_no_bp
			# Time
			if (cs_round["winner_name"] == team_name_1) : 
				win_time_1 += cs_round["end_seconds"] - cs_round["start_seconds"]
			else : 
				win_time_2 += cs_round["end_seconds"] - cs_round["start_seconds"]
			round_index += 1
		# In case key values are null, just skip the game
		# This also avoids to make divisions by 0
		if date_str == "" \
		or map_name == "" \
		or score_team_1 == 0 \
		or score_team_2 == 0 \
		or team_name_1 == "" \
		or team_name_2 == "" \
		or kill_count_1 == 0 \
		or kill_count_2 == 0 \
		or round_index < Stats.min_round_count :
			raise NameError("Missing key value")
		# Compute features
		# killPerDeath
		f_kpd_1 = kill_count_1 / death_count_1
		f_kpd_2 = kill_count_2 / death_count_2
		# headShotGiven
		f_hs_given_1 = hs_count_1 / kill_count_1
		f_hs_given_2 = hs_count_2 / kill_count_2
		# headShotTaken
		f_hs_taken_1 = f_hs_given_2
		f_hs_taken_2 = f_hs_given_1
		# tradeKillGiven
		f_tk_given_1 = tk_count_1 / kill_count_2
		f_tk_given_2 = tk_count_2 / kill_count_1
		# tradeKillTaken
		f_tk_taken_1 = f_tk_given_2
		f_tk_taken_2 = f_tk_given_1
		# ecoRoundWin
		f_eco_win_1 = (eco_win_count_1 / eco_count_1 if eco_count_1 > 0 else 0)
		f_eco_win_2 = (eco_win_count_2 / eco_count_2 if eco_count_2 > 0 else 0)
		# ecoRoundLose
		f_eco_lose_1 = f_eco_win_2
		f_eco_lose_2 = f_eco_win_1
		# bombPlantGiven
		f_bp_given_1 = (bp_count_1 / ct_round_count_2 if ct_round_count_2 > 0 else 0)
		f_bp_given_2 = (bp_count_2 / ct_round_count_1 if ct_round_count_1 > 0 else 0)
		# bombPlantTaken
		f_bp_taken_1 = f_bp_given_2
		f_bp_taken_2 = f_bp_given_1
		# bombExplosionGiven
		f_be_given_1 = (be_count_1 / ct_round_count_2 if ct_round_count_2 > 0 else 0)
		f_be_given_2 = (be_count_2 / ct_round_count_1 if ct_round_count_1 > 0 else 0)
		# bombExplosionTaken
		f_be_taken_1 = f_be_given_2
		f_be_taken_2 = f_be_given_1
		# timeBombPlant
		f_bp_time_given_1 = (bp_time_1 / ct_round_count_2 if ct_round_count_2 > 0 else Stats.time_penality_no_bp)
		f_bp_time_given_2 = (bp_time_2 / ct_round_count_1 if ct_round_count_1 > 0 else Stats.time_penality_no_bp)
		# timeBombPlant
		f_bp_time_taken_1 = f_bp_time_given_2
		f_bp_time_taken_2 = f_bp_time_given_1
		# timeRoundWin = rounds:i:end_seconds - rounds:i:start_seconds
		f_time_win_1 = (win_time_1 / score_team_1 if score_team_1 > 0 else Stats.time_round + Stats.time_bomb_explosion)
		f_time_win_2 = (win_time_2 / score_team_2 if score_team_2 > 0 else Stats.time_round + Stats.time_bomb_explosion)
		# timeRoundLose
		f_time_lose_1 = f_time_win_2
		f_time_lose_2 = f_time_win_1
		# playerSkillAverage
		f_skill_1 = statistics.mean(players_skill_1)
		f_skill_2 = statistics.mean(players_skill_2)
		# playerSkillSDeviation
		f_skill_sd_1 = statistics.stdev(players_skill_1, f_skill_1)
		f_skill_sd_2 = statistics.stdev(players_skill_2, f_skill_2)		
		# finalScore = score_team1 - score_team2
		f_score_1 = score_team_1 - score_team_2
		f_score_2 = score_team_2 - score_team_1
		# finalWin
		f_win_1 = (1 if f_score_1 > 0 else 0)
		f_win_2 = (1 if f_score_2 > 0 else 0)
		# Add in data structure
		features_1 = [f_kpd_1, f_hs_given_1, f_hs_taken_1, f_tk_given_1, f_tk_taken_1, f_eco_win_1, f_eco_lose_1, f_bp_given_1, f_bp_taken_1, \
		f_be_given_1, f_be_taken_1, f_bp_time_given_1, f_bp_time_taken_1, f_time_win_1, f_time_lose_1, f_skill_1, f_skill_sd_1, f_score_1, f_win_1]
		features_2 = [f_kpd_2, f_hs_given_2, f_hs_taken_2, f_tk_given_2, f_tk_taken_2, f_eco_win_2, f_eco_lose_2, f_bp_given_2, f_bp_taken_2, \
		f_be_given_2, f_be_taken_2, f_bp_time_given_2, f_bp_time_taken_2, f_time_win_2, f_time_lose_2, f_skill_2, f_skill_sd_2, f_score_2, f_win_2]
		return GameStats(date=datetime.datetime.strptime(date_str, '%d-%m-%Y'), map=map_name, teams=[team_name_1,team_name_2], features=[features_1, features_2], winner=f_win_1)

	def get_features_history(self, game_index, team_name, num_lookback_games, num_lookback_days) :
		"""Compute the features"""
		# TODO: use hyperparameters to weight features' history (for now it is a simple average)
		sum_features = numpy.zeros(Stats.number_of_features)
		for team in self.teams :
			if team.name == team_name :
				num_game = team.games.index(game_index)
				# If not enough previous games to create a feature history, raise an error (games are ordered chronologically)
				if num_game < num_lookback_games :
					raise ValueError("Not enough previous games to average within num_lookback_games ({0}).".format(num_lookback_games))
				# Get game's date
				date = self.games[game_index].date
				# Iterate over previous games of this team
				for game_id in team.games[num_game-num_lookback_games:num_game] :
					game = self.games[game_id]
					# If games are too old, raise an error
					if (game.date - date).days > num_lookback_days :
						raise ValueError("Not enough previous games to average within num_lookback_days ({0}).".format(num_lookback_days))
					# Get right set of features depending on the team
					feature_id = 0 if game.teams[0] == team.name else 1
					sum_features += game.features[feature_id]
		# Return the average of features from history
		return sum_features / num_lookback_games
