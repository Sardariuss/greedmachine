﻿using System;
using System.Linq;
using Core.Models;
using Services.Concrete;
using GalaSoft.MvvmLight.Threading;

namespace JSONDemoExtractor
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.Title = "JSONDemoExtractor";

            if (args.Count() == 2)
            {
                try
                {
                    string demoPath = args[0];
                    string folderPath = args[1];

                    //Console.WriteLine("JSONDemoExtractor: extracting JSON file...");

                    DispatcherHelper.Initialize();

                    DemosService demosService = new DemosService();

                    Demo demo = demosService.GetDemoHeaderAsync(demoPath);

                    demosService.AnalyzeDemo(demo);

                    demosService._cacheService.GenerateJsonAsync(demo, folderPath);

                    //Console.WriteLine("JSONDemoExtractor: JSON file extracted.");

                    return 0;
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("JSONDemoExtractor: exception while extracting JSON file: \n" + e.ToString());
                    return 1;
                }
            }
            else
            {
                Console.Error.WriteLine("JSONDemoExtractor expects 2 arguments: the path of the .dem file to read and the path of the JSON file to write.");
                return 2;
            }
        }
    }
}
