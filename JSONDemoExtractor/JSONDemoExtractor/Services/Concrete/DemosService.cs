﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using Core;
using Core.Models;
using Core.Models.protobuf;
using Core.Models.Serialization;
using Core.Models.Source;
using ICSharpCode.SharpZipLib.BZip2;
using Newtonsoft.Json;
using ProtoBuf;
using Services.Concrete.Analyzer;
using Services.Interfaces;
using Demo = Core.Models.Demo;
using Player = Core.Models.Player;

namespace Services.Concrete
{
	public class DemosService : IDemosService
	{
		public string DownloadFolderPath { get; set; }

		public long SelectedStatsAccountSteamId { get; set; }

		public bool ShowOnlyAccountDemos { get; set; } = false;

		public bool IgnoreLaterBan { get; set; }

		public readonly CacheService _cacheService = new CacheService();

		public DemosService(){}

		/// <summary>
		/// Check if there are banned players and update their banned flags
		/// </summary>
		/// <param name="demo"></param>
		/// <returns></returns>
		public Demo AnalyzeBannedPlayersAsync(Demo demo)
		{
            // Greedy modification
            demo.HasCheater = false;
			return demo;
		}

		public Demo GetDemoHeaderAsync(string demoFilePath)
		{
            Demo demo = DemoAnalyzer.ParseDemoHeader(demoFilePath);
			if (demo != null)
			{
				// Update the demo name and path because it may be renamed / moved
				demo.Name = Path.GetFileName(demoFilePath);
				demo.Path = demoFilePath;
			}

			return demo;
		}

		public Demo GetDemoDataAsync(Demo demo)
		{
			// If demo is in cache we retrieve its data
			bool demosIsCached = _cacheService.HasDemoInCache(demo);
			if (demosIsCached)
			{
				demo = _cacheService.GetDemoDataFromCache(demo.Id);
				demo.Source = Source.Factory(demo.SourceName);
			}
			return demo;
		}

		public List<Demo> GetDemosHeader(List<string> folders, List<Demo> currentDemos = null, bool limit = false)
		{
			List<Demo> demos = new List<Demo>();
			List<Demo> demoKeeped = new List<Demo>();

			if (folders.Count > 0)
			{
				foreach (string folder in folders)
				{
					if (Directory.Exists(folder))
					{
						string[] files = Directory.GetFiles(folder, "*.dem");
						foreach (string file in files)
						{
							if (file.Contains("myassignedcase")) continue;
							Demo demo = GetDemoHeaderAsync(file);
							if (demo != null)
							{
								if (currentDemos != null && currentDemos.Contains(demo))
									continue;

								demo = GetDemoDataAsync(demo);

								if (ShowOnlyAccountDemos && SelectedStatsAccountSteamId != 0 && demo.Players.FirstOrDefault(p => p.SteamId == SelectedStatsAccountSteamId) == null)
									continue;

								if (currentDemos == null || !currentDemos.Contains(demo))
								{
									demos.Add(demo);
								}
							}
						}
						// sort by date
						demos.Sort((d1, d2) => d2.Date.CompareTo(d1.Date));
						if (limit)
						{
							// keep the 50 first
							demoKeeped = demos.Take(AppSettings.DEMO_PAGE_COUNT).ToList();
							for (int i = 0; i < demoKeeped.Count; i++)
							{
								demoKeeped[i] = GetDemoDataAsync(demoKeeped[i]);
							}
						}
					}
				}
			}

			return limit ? demoKeeped : demos;
		}

		public Demo AnalyzeDemo(Demo demo)
		{
			if (!File.Exists(demo.Path))
			{
				// Demo may be moved to an other folder, just clear cache
				_cacheService.RemoveDemo(demo);
			}

			DemoAnalyzer analyzer = DemoAnalyzer.Factory(demo);

			demo = analyzer.AnalyzeDemoAsync();

			return demo;
		}

		public void SaveComment(Demo demo, string comment)
		{
			demo.Comment = comment;
			_cacheService.WriteDemoDataCache(demo);
		}

		public void SaveStatus(Demo demo, string status)
		{
			demo.Status = status;
			_cacheService.WriteDemoDataCache(demo);
		}

		public void SetSource(ObservableCollection<Demo> demos, string source)
		{
			foreach (Demo demo in demos.Where(demo => demo.Type != "POV"))
			{
				switch (source)
				{
					case "valve":
						demo.Source = new Valve();
						break;
					case "esea":
						demo.Source = new Esea();
						break;
					case "ebot":
						demo.Source = new Ebot();
						break;
					case "faceit":
						demo.Source = new Faceit();
						break;
					case "cevo":
						demo.Source = new Cevo();
						break;
					default:
						demo.Source = new Valve();
						break;
				}
				_cacheService.WriteDemoDataCache(demo);
			}
		}

		public Demo AnalyzePlayersPosition(Demo demo)
		{
			if (!File.Exists(demo.Path))
			{
				// Demo may be moved to an other folder, just clear cache
				_cacheService.RemoveDemo(demo);
			}

			DemoAnalyzer analyzer = DemoAnalyzer.Factory(demo);
			analyzer.AnalyzePlayersPosition = true;

			demo = analyzer.AnalyzeDemoAsync();

			_cacheService.WriteDemoDataCache(demo);

			return demo;
		}

		/// <summary>
		/// Return demos from JSON backup file
		/// </summary>
		/// <returns></returns>
		public List<Demo> GetDemosFromBackup(string jsonFile)
		{
			List<Demo> demos = new List<Demo>();
			if (File.Exists(jsonFile))
			{
				string json = File.ReadAllText(jsonFile);
				demos = JsonConvert.DeserializeObject<List<Demo>>(json, new DemoListBackupConverter());
			}
			return demos;
		}

		public Rank GetLastRankAccountStatsAsync(long steamId)
		{
			Rank lastRank = AppSettings.RankList[0];
			List<string> folders = _cacheService.GetFoldersAsync();
			// Get only the demos header to have access to the demo's date
			List<Demo> demos = GetDemosHeader(folders);
			if (demos.Any())
			{
				if (steamId == 0) steamId = SelectedStatsAccountSteamId;
				if (steamId == 0) return lastRank;
				// keep only demos from valve where the account played
				List<Demo> newDemoList = demos.Where(d => d.SourceName == "valve"
				&& d.Players.FirstOrDefault(p => p.SteamId == steamId) != null)
				.ToList();
				if (newDemoList.Any())
				{
					// Sort by date and keep the more recent
					newDemoList.Sort((d1, d2) => d1.Date.CompareTo(d2.Date));
					Demo lastDemo = newDemoList.Last();
					// Get the new rank
					Player player = lastDemo.Players.First(p => p.SteamId == steamId);
					lastRank = AppSettings.RankList.FirstOrDefault(r => r.Number == player.RankNumberNew);
					// Save it to the cache
					RankInfo rankInfo = new RankInfo
					{
						Number = player.RankNumberNew,
						SteamId = player.SteamId,
						LastDate = lastDemo.Date
					};
					_cacheService.SaveLastRankInfoAsync(rankInfo);
				}
			}

			return lastRank;
		}

		public List<Demo> GetDemosPlayer(string steamId)
		{
			List<Demo> result = new List<Demo>();

			List<Demo> demos = _cacheService.GetDemoListAsync();

			if (demos.Any())
			{
				result = demos.Where(demo => demo.Players.FirstOrDefault(p => p.SteamId.ToString() == steamId) != null).ToList();
				for(int i = 0; i < result.Count; i++)
				{
					Demo demo = GetDemoHeaderAsync(result[i].Path);
					if (demo != null) result[i] = demo;
				}
			}

			return result;
		}

		public bool DeleteDemo(Demo demo)
		{
			if (!File.Exists(demo.Path)) return false;
			_cacheService.RemoveDemo(demo);
			if (File.Exists(demo.Path + ".info")) File.Delete(demo.Path + ".info");
			File.Delete(demo.Path);

			return true;
		}

		public bool DownloadDemo(string url, string demoName)
		{
			using (WebClient webClient = new WebClient())
			{
				try
				{
					string location = DownloadFolderPath + Path.DirectorySeparatorChar + demoName + ".bz2";
					Uri uri = new Uri(url);
					webClient.DownloadFile(uri, location);
					return true;
				}
				catch (Exception e)
				{
					Logger.Instance.Log(e);
					return false;
				}
			}
		}

		public bool DecompressDemoArchive(string demoName)
		{
			string archivePath = DownloadFolderPath + Path.DirectorySeparatorChar + demoName + ".bz2";
			string destination = DownloadFolderPath + Path.DirectorySeparatorChar + demoName + ".dem";
			if (!File.Exists(archivePath)) return false;
			BZip2.Decompress(File.OpenRead(archivePath), File.Create(destination), true);
			File.Delete(archivePath);
			return true;
		}

		public Dictionary<string, string> GetDemoListUrl()
		{
			Dictionary<string, string> demoUrlList = new Dictionary<string, string>();

			string filePath = AppSettings.GetMatchListDataFilePath();
			if (File.Exists(filePath))
			{
				using (FileStream file = File.OpenRead(filePath))
				{
					try
					{
						CMsgGCCStrike15_v2_MatchList matchList = Serializer.Deserialize<CMsgGCCStrike15_v2_MatchList>(file);
						foreach (CDataGCCStrike15_v2_MatchInfo matchInfo in matchList.matches)
						{
							// old definition
							if (matchInfo.roundstats_legacy != null)
							{
								CMsgGCCStrike15_v2_MatchmakingServerRoundStats roundStats = matchInfo.roundstats_legacy;
								ProcessRoundStats(matchInfo, roundStats, demoUrlList);
							}
							else
							{
								// new definition
								List<CMsgGCCStrike15_v2_MatchmakingServerRoundStats> roundStatsList = matchInfo.roundstatsall;
								foreach (CMsgGCCStrike15_v2_MatchmakingServerRoundStats roundStats in roundStatsList)
								{
									ProcessRoundStats(matchInfo, roundStats, demoUrlList);
								}
							}
						}
					}
					catch (Exception e)
					{
						Logger.Instance.Log(e);
					}
				}
			}

			return demoUrlList;
		}

		/// <summary>
		/// Check if the round stats contains useful information and in this case do the work
		/// 1. Check if the demo archive is still available
		/// 2. Create the .info file
		/// 3. Add the demo to the download list
		/// </summary>
		/// <param name="matchInfo"></param>
		/// <param name="roundStats"></param>
		/// <param name="demoUrlList"></param>
		/// <returns></returns>
		private void ProcessRoundStats(CDataGCCStrike15_v2_MatchInfo matchInfo, CMsgGCCStrike15_v2_MatchmakingServerRoundStats roundStats, Dictionary<string, string> demoUrlList)
		{
			string demoName = GetDemoName(matchInfo, roundStats);
			if (roundStats.reservationid != 0 && roundStats.map != null)
			{
				if (IsDownloadRequired(demoName, roundStats.map))
				{
					if (SerializeMatch(matchInfo, demoName))
					{
						demoUrlList.Add(demoName, roundStats.map);
					}
				}
			}
		}

		/// <summary>
		/// Create the .info file
		/// </summary>
		/// <param name="matchInfo"></param>
		/// <param name="demoName"></param>
		/// <returns></returns>
		private bool SerializeMatch(CDataGCCStrike15_v2_MatchInfo matchInfo, string demoName)
		{
			string infoFilePath = DownloadFolderPath + Path.DirectorySeparatorChar + demoName + ".dem.info";
			try
			{
				using (FileStream fs = File.Create(infoFilePath))
				{
					Serializer.Serialize(fs, matchInfo);
				}
			}
			catch (Exception e)
			{
				Logger.Instance.Log(e);
				return false;
			}

			return true;
		}

		/// <summary>
		/// Return the demo name
		/// </summary>
		/// <param name="matchInfo"></param>
		/// <param name="roundStats"></param>
		/// <returns></returns>
		private static string GetDemoName(CDataGCCStrike15_v2_MatchInfo matchInfo,
			CMsgGCCStrike15_v2_MatchmakingServerRoundStats roundStats)
		{
			return "match730_" + string.Format("{0,21:D21}", roundStats.reservationid) + "_"
				+ string.Format("{0,10:D10}", matchInfo.watchablematchinfo.tv_port) + "_"
				+ matchInfo.watchablematchinfo.server_ip;
		}

		/// <summary>
		/// Check if:
		/// - The demo archive is still available
		/// - The .dem and .info files already exists
		/// </summary>
		/// <param name="demoName"></param>
		/// <param name="demoArchiveUrl"></param>
		/// <returns></returns>
		private bool IsDownloadRequired(string demoName, string demoArchiveUrl)
		{
			bool result = CheckIfArchiveIsAvailable(demoArchiveUrl);
			string[] fileList = new DirectoryInfo(DownloadFolderPath).GetFiles().Select(o => o.Name).ToArray();
			if (fileList.Contains(demoName + ".dem") && fileList.Contains(demoName + ".dem.info")) result = false;

			return result;
		}

		/// <summary>
		/// Check if the URL doesn't return a 404
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		private static bool CheckIfArchiveIsAvailable(string url)
		{
			try
			{
				HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
				request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
				response.Close();
				return response.StatusCode == HttpStatusCode.OK;
			}
			catch
			{
				return false;
			}
		}
	}
}
