﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.Models;
using Demo = Core.Models.Demo;

namespace Services.Interfaces
{
	public interface IDemosService
	{
		/// <summary>
		/// Path where demos will be saved
		/// </summary>
		string DownloadFolderPath { get; set; }

		/// <summary>
		/// Selected account SteamID to focus on
		/// </summary>
		long SelectedStatsAccountSteamId { get; set; }

		/// <summary>
		/// Flag to focus on selected SteamID
		/// </summary>
		bool ShowOnlyAccountDemos { get; set; }

		bool IgnoreLaterBan { get; set; }

		/// <summary>
		/// Return demo's header
		/// </summary>
		/// <param name="demoFilePath"></param>
		/// <returns></returns>
		Demo GetDemoHeaderAsync(string demoFilePath);

		/// <summary>
		/// Return only demos header
		/// </summary>
		/// <param name="folders"></param>
		/// <param name="currentDemos"></param>
		/// <param name="limit"></param>
		/// <param name="accountSteamId"></param>
		/// <returns></returns>
		List<Demo> GetDemosHeader(List<string> folders, List<Demo> currentDemos = null, bool limit = false);

		/// <summary>
		/// Return the whole demo
		/// </summary>
		/// <param name="demo"></param>
		/// <returns></returns>
		Demo GetDemoDataAsync(Demo demo);

		/// <summary>
		/// Analyze the demo passed on parameter
		/// </summary>
		/// <param name="demo"></param>
		/// <param name="token"></param>
		/// <returns></returns>
		Demo AnalyzeDemo(Demo demo);

		/// <summary>
		/// Save the demo's comment
		/// </summary>
		/// <param name="demo"></param>
		/// <param name="comment"></param>
		/// <returns></returns>
		void SaveComment(Demo demo, string comment);

		/// <summary>
		/// Save the demo's status
		/// </summary>
		/// <param name="demo"></param>
		/// <param name="status"></param>
		/// <returns></returns>
		void SaveStatus(Demo demo, string status);

		void SetSource(ObservableCollection<Demo> demos, string source);

		Demo AnalyzePlayersPosition(Demo demo);

		List<Demo> GetDemosFromBackup(string jsonFile);

		Demo AnalyzeBannedPlayersAsync(Demo demo);

		/// <summary>
		/// Return the last rank detected for the specific steamId
		/// </summary>
		/// <param name="steamId"></param>
		/// <returns></returns>
		Rank GetLastRankAccountStatsAsync(long steamId);

		/// <summary>
		/// Return demos that contains the SteamID
		/// </summary>
		/// <param name="steamId"></param>
		/// <returns></returns>
		List<Demo> GetDemosPlayer(string steamId);

		/// <summary>
		/// Delete a demo from file system
		/// </summary>
		/// <param name="demo"></param>
		/// <returns></returns>
		bool DeleteDemo(Demo demo);

		/// <summary>
		/// Return the list of demos that need to be downloaded
		/// demo name => demo URL 
		/// </summary>
		/// <returns></returns>
		Dictionary<string, string> GetDemoListUrl();

		/// <summary>
		/// Download the demo archive
		/// </summary>
		/// <param name="url">Url of the demo archive</param>
		/// <param name="location">Location where the archive will be saved</param>
		/// <returns></returns>
		bool DownloadDemo(string url, string location);

		/// <summary>
		/// Decompress the demo archive
		/// </summary>
		/// <param name="demoName"></param>
		/// <returns></returns>
		bool DecompressDemoArchive(string demoName);
	}
}
