﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Core.Models;
using Core.Models.Events;

namespace Services.Interfaces
{
	public interface ICacheService
	{
		bool HasDemoInCache(Demo demo);

		Demo GetDemoDataFromCache(string demoId);

		void WriteDemoDataCache(Demo demo);

		bool AddSuspectToCache(string suspectSteamCommunityId);

		List<string> GetSuspectsListFromCache();

		bool RemoveSuspectFromCache(string steamId);

		void ClearDemosFile();

		void CreateBackupCustomDataFile(string filePath);

		bool ContainsDemos();

		bool AddSteamIdToBannedList(string steamId);

		List<string> GetSuspectsBannedList();

		bool AddAccountAsync(Account account);

		bool RemoveAccountAsync(Account account);

		List<Account> GetAccountListAsync();

		Account GetAccountAsync(long steamId);

		List<string> GetFoldersAsync();

		bool AddFolderAsync(string path);

		bool RemoveFolderAsync(string path);

		/// <summary>
		/// Return all the demos inside each folders
		/// </summary>
		/// <returns></returns>
		List<Demo> GetDemoListAsync(bool isShowAllFolders, bool limitStatsFolder, string lastFolder);

		List<Demo> GetFilteredDemoListAsync(DateTime dateFrom, DateTime dateTo, bool isShowAllFolders, bool limitStatsFolder, string lastFolder);

		List<string> GetPlayersWhitelist();

		bool AddPlayerToWhitelist(string suspectSteamCommunityId);

		bool RemovePlayerFromWhitelist(string steamId);

		/// <summary>
		/// Export demos to JSON files
		/// </summary>
		/// <param name="demo"></param>
		/// <param name="folderPath"></param>
		/// <returns></returns>
		bool GenerateJsonAsync(Demo demo, string folderPath);

		/// <summary>
		/// Return the size of the cache folder
		/// </summary>
		/// <returns></returns>
		long GetCacheSizeAsync();

		/// <summary>
		/// Remove demo from cache
		/// </summary>
		/// <param name="demo"></param>
		/// <returns></returns>
		bool RemoveDemo(Demo demo);

		/// <summary>
		/// Return the list of WeaponFire events of the demo
		/// </summary>
		/// <param name="demo"></param>
		/// <returns></returns>
		ObservableCollection<WeaponFireEvent> GetDemoWeaponFiredAsync(Demo demo);

		/// <summary>
		/// Return the list of PlayerBlindedEvent of the demo
		/// </summary>
		/// <param name="demo"></param>
		/// <returns></returns>
		ObservableCollection<PlayerBlindedEvent> GetDemoPlayerBlindedAsync(Demo demo);

		/// <summary>
		/// Return the last RankInfo detected for the selected account
		/// </summary>
		/// <returns></returns>
		RankInfo GetLastRankInfoAsync(long steamId);

		/// <summary>
		/// Return the last Rank detected for the steamId
		/// </summary>
		/// <returns></returns>
		Rank GetLastRankAsync(long steamId);

		/// <summary>
		/// Save the RankInfo
		/// </summary>
		/// <param name="rankInfo"></param>
		/// <returns></returns>
		bool SaveLastRankInfoAsync(RankInfo rankInfo);

		/// <summary>
		/// Return all RankInfo
		/// </summary>
		/// <returns></returns>
		List<RankInfo> GetRankInfoListAsync();

		/// <summary>
		/// Update the RankInfo if it's necessary (based on the demo's data)
		/// </summary>
		/// <param name="demo"></param>
		/// <param name="steamId"></param>
		/// <returns></returns>
		bool UpdateRankInfoAsync(Demo demo, long steamId);

		/// <summary>
		/// Clear the RankInfo list
		/// </summary>
		/// <returns></returns>
		void ClearRankInfoAsync();

		/// <summary>
		/// Remove a specific RankInfo based on the SteamID
		/// </summary>
		/// <param name="steamId"></param>
		/// <returns></returns>
		bool RemoveRankInfoAsync(long steamId);

		/// <summary>
		/// Remove all vdm files within all folders
		/// </summary>
		/// <returns></returns>
		bool DeleteVdmFiles();

		/// <summary>
		/// Check if the dummy cache file exists
		/// </summary>
		/// <returns></returns>
		bool HasDummyCacheFile();

		/// <summary>
		/// Delete the dummy cache file
		/// </summary>
		/// <returns></returns>
		void DeleteDummyCacheFile();
	}
}