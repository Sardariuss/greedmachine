
class TeamStats :

	def __init__(self, team_dict=None, name="", games=None) :
		"""Constructor"""
		if team_dict!=None :
			self.from_dict(team_dict)
		else :
			self.name = name
			self.games = games if games!=None else []

	def to_dict(self) :
		"""Initialize team stats from dictionary"""
		return {"name" : self.name, "games" : self.games}

	def from_dict(self, team_dict) :
		"""Return team stats dictionary"""
		self.name = team_dict["name"]
		self.games = team_dict["games"]
		