# This will be the file with the main program

#that:

#runs extract_features
#runs create_model / tune_model
#runs run_example

import os.path
import numpy
from sklearn.linear_model import LogisticRegression
from stats import Stats
from model import Model

if __name__ == "__main__" :

	DATA_DIR = 'C:\\Users\\Tanguy\\Google Drive\\GreedMachine\\sandbox\\data'
	STATS_FILE = 'C:\\Users\\Tanguy\\Google Drive\\GreedMachine\\sandbox\\stats.json'
	NUM_LOOKBACK_GAMES = 5
	NUM_LOOKBACK_DAYS = 30
	TRAINING_RATIO = 0.7

	# Extract the stats from directory DATA_DIR, 
	# or load them from backup file STATS_FILE file if any
	stats = Stats()
	if os.path.isfile(STATS_FILE) :
		stats.load(STATS_FILE)
	else :
		stats.extract_from_files(DATA_DIR)
		stats.dump(STATS_FILE)
	
	# Build the model
	model = Model()
	model.build(stats, NUM_LOOKBACK_GAMES, NUM_LOOKBACK_DAYS)
	# Split the model between train samples and test samples
	train_X = numpy.array(model.X[:(int)(TRAINING_RATIO*len(model.X))])
	train_y = numpy.array(model.y[:(int)(TRAINING_RATIO*len(model.y))])
	test_X = numpy.array(model.X[(int)(TRAINING_RATIO*len(model.X)):])
	test_y = numpy.array(model.y[(int)(TRAINING_RATIO*len(model.y)):])
	# Run the logistic regression
	logistic_regression = LogisticRegression()
	logistic_regression.fit(train_X, train_y)
	print("Score of logistic regression : %f" % logistic_regression.score(test_X, test_y))
	