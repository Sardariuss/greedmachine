## Synopsis

The GreedMachine is a Python console application which aims to give a reliable indicator to bet on future CS:GO professional matches.

## Code Example

See run.py

## Motivation

I was searching for a concrete machine learning driven application which I could develop from end-to-end. 

The intial idea of using machine learning for betting came from this article: http://blog.sigopt.com/post/136340340198/sigopt-for-ml-using-model-tuning-to-beat-vegas

I chose to bet on CS:GO because the data is free, abundant and up-to-date which makes it a very good fit for ML applications.

## Installation

Runs with Python 3.5

For the machine learning part installing Numpy, SkLearn and joblib via pip should be enough. Data will be available soon so one does not need to scrape all of it.

The scraper addionally requires Python Selenium, PhantomJS and most of the dependencies listed here: https://github.com/akiver/CSGO-Demos-Manager#dependencies

## CS:GO Demo Manager

The scraper's JSONDemoExtractor is basically a fork of the CSGODemoManager (https://github.com/akiver/CSGO-Demos-Manager). All rights and credits goes to Akiver (https://github.com/akiver/).
