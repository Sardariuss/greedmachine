import datetime
import numpy
import traceback

class Model :

	def __init__(self) :
		"""Constructor"""
		# matrix of inputs X, a row is an element
		self.X = []
		# vector of outputs y
		self.y = []

	def build(self, stats, num_lookback_games, num_lookback_days) :
		"""Build the model from the given stats"""
		del self.X[:]
		del self.y[:]
		for game_index, game in enumerate(stats.games) :
			try :
				self.X.append(numpy.concatenate([
					stats.get_features_history(game_index, game.teams[0], num_lookback_games, num_lookback_days),
					stats.get_features_history(game_index, game.teams[1], num_lookback_games, num_lookback_days)]))
				self.y.append(game.winner)
				print("Game %d (%sVS%s) has been successfully added to the model." % (game_index, game.teams[0], game.teams[1]))
			except ValueError as e :
				print(e)
			except :
				print(traceback.format_exc())
	